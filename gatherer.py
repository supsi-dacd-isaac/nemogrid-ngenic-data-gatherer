import logging
import requests
import json
import sys
import argparse
import os
import datetime
import pytz
from http import HTTPStatus

from influxdb import InfluxDBClient

# --------------------------------------------------------------------------- #
# Constants
# --------------------------------------------------------------------------- #
DT_FORMAT = '%Y-%m-%d %H:%M:%S'
DT_FORMAT_REMOTE_QUERY = '%Y-%m-%dT%H:%M:%S.0Z'
DT_FORMAT_REMOTE_RESPONSE = '%Y-%m-%dT%H:%M:%S.%fZ'

# --------------------------------------------------------------------------- #
# Functions
# --------------------------------------------------------------------------- #
def request_new_token():
    logger.info('Request a new token')
    body = {
                'grant_type': 'client_credentials',
                'client_id': cfg['remoteService']['clientId'],
                'client_secret': cfg['remoteService']['clientSecret']
            }

    try:
        url = cfg['remoteService']['baseUrl'] + "/oauth/token"
        response = requests.post(url, json=body, timeout=cfg['remoteService']['requestTimeout'])

        if(response.status_code != HTTPStatus.OK):
            logging.error('Code: %i, message: %s' % response.status_code, response.text)
            logging.error('Unable to get a new token')
            logger.info("Exit program")
            sys.exit()

        token = response.json()["access_token"]

        # write the new token on file
        with open(cfg['utils']['tokenFile'], 'w') as of:
            json.dump({'token': token}, of, indent=4)

    except Exception as e:
        logger.error('EXCEPTION: %s' % str(e))
        logging.error('Unable to get a new token')
        logger.info("Exit program")
        sys.exit()
    return token

def get_token():
    try:
        with open(cfg['utils']['tokenFile']) as json_file:
            data = json.load(json_file)
            tkn = data['token']
            logger.info('Got token %s' % tkn)
    except Exception as e:
        logger.error('EXCEPTION: %s' % str(e))
        tkn = request_new_token()

    return tkn

def get_value(url,token=None, body = {},query={}):
    #Get a value
    auth_header = {"Authorization": "Bearer " + token} #demand working token refreash when neede
    response = requests.get(cfg['remoteService']['baseUrl'] + url, json=body, headers=auth_header, params=query,
                            timeout=cfg['remoteService']['requestTimeout'])
    if response.status_code != HTTPStatus.OK:
        logging.error('Code: %i, message: %s' % (response.status_code, response.text))
        logging.error('Unable to get a the value')
        return None
    else:
        return response.json()

def define_period(cfg_period):
    start = None

    tz_local = pytz.timezone(cfg['utils']['timeZone'])

    # 'last' case
    if 'last' in cfg_period['type']:
        end = datetime.datetime.utcnow() + datetime.timedelta(hours=1)

        stuff, num = cfg_period['type'][0:-1].split('_')
        num = int(num)
        # it works only for days, hours and minutes
        if cfg_period['type'][-1] == 'd':
            start = datetime.datetime.now() - datetime.timedelta(days=num)
        elif cfg_period['type'][-1] == 'h':
            start = datetime.datetime.now() - datetime.timedelta(hours=num)
        elif cfg_period['type'][-1] == 'm':
            start = datetime.datetime.now() - datetime.timedelta(minutes=num)

    # 'custom' case
    else:
        start = datetime.datetime.strptime(cfg_period['customPars']['start'], DT_FORMAT)
        end = datetime.datetime.strptime(cfg_period['customPars']['end'], DT_FORMAT)

        # check if the custom timestamps are correct (i.e. start<end)
        if start.timestamp() > end.timestamp():
            return None, None

    return start.astimezone(tz_local), end.astimezone(tz_local)

def gather_data(start_dt, end_dt):
    dps = []
    for station in stations:
        querystring = {
                        'from_datetime': datetime.datetime.strftime(start_dt, DT_FORMAT_REMOTE_QUERY),
                        'to_datetime': datetime.datetime.strftime(end_dt, DT_FORMAT_REMOTE_QUERY),
                        'substation_name': station['name']
                      }
        values = get_value('/substation/measurements', token, query=querystring)

        for value in values:
            ts = datetime.datetime.strptime(value['time'], DT_FORMAT_REMOTE_RESPONSE)
            ts += datetime.timedelta(hours=int(cfg["utils"]["dtimeHours"]))
            point = {
                        'time': int(ts.timestamp()),
                        'measurement': cfg['influxDB']['measurement'],
                        'fields': dict(value=float(value['power_kw'])),
                        'tags': dict(station=station['name'], substation_id=value['substation_id'], signal='P_kW')
                    }
            dps.append(point)

            # check if the datapoints array is long enough
            if len(dps) >= cfg['influxDB']['maxLinesToInsert']:
                # send a complete batch to InfluxDB
                influx_client.write_points(dps, time_precision=cfg['influxDB']['timePrecision'])
                logger.info('Inserted %i points in InfluxDB' % len(dps))
                dps = []

    # send a the remaining data to InfluxDB
    influx_client.write_points(dps, time_precision=cfg['influxDB']['timePrecision'])
    logger.info('Inserted %i points in InfluxDB' % len(dps))


# --------------------------------------------------------------------------- #
# Main
# --------------------------------------------------------------------------- #
if __name__ == "__main__":
    # --------------------------------------------------------------------------- #
    # Configuration file
    # --------------------------------------------------------------------------- #
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-c", help="configuration file")
    arg_parser.add_argument("-l", help="log file (optional, if empty log redirected on stdout)")
    args = arg_parser.parse_args()

    # Load the main parameters
    config_file = args.c
    if os.path.isfile(config_file) is False:
        print('\nATTENTION! Unable to open configuration file %s\n' % config_file)
        sys.exit(1)

    cfg = json.loads(open(args.c).read())

    # --------------------------------------------------------------------------- #
    # Set logging object
    # --------------------------------------------------------------------------- #
    if not args.l:
        log_file = None
    else:
        log_file = args.l

    logger = logging.getLogger()
    logging.basicConfig(format='%(asctime)-15s::%(levelname)s::%(funcName)s::%(message)s', level=logging.INFO,
                        filename=log_file)

    # --------------------------------------------------------------------------- #
    # Starting program
    # --------------------------------------------------------------------------- #
    logger.info('Starting program')

    # --------------------------------------------------------------------------- #
    # InfluxDB connection
    # --------------------------------------------------------------------------- #
    logger.info('Connection to InfluxDb server on socket [%s:%s]' % (cfg['influxDB']['host'], cfg['influxDB']['port']))
    try:
        influx_client = InfluxDBClient(host=cfg['influxDB']['host'], port=cfg['influxDB']['port'],
                                       password=cfg['influxDB']['password'], username=cfg['influxDB']['user'],
                                       database=cfg['influxDB']['database'], ssl=cfg['influxDB']['ssl'])
    except Exception as e:
        logger.error('EXCEPTION: %s' % str(e))
        sys.exit(3)
    logger.info('Connection successful')

    logging.basicConfig(level=logging.INFO, format='%(levelname)-8s %(message)s')

    # get the start/end date
    [start_dt, end_dt] = define_period(cfg['period'])

    # get the first token
    token = get_token()

    # get stations list
    stations = get_value('/substation', token)
    if stations is None:
        # request a new token and get
        token = request_new_token()

        # get stations list again
        stations = get_value('/substation', token)
        if stations is None:
            logger.error('Unable to get data from the Ngenic API')
        else:
            # collect the data of all the stations
            gather_data(start_dt, end_dt)
    else:
        # collect the data of all the stations
        gather_data(start_dt, end_dt)

    # --------------------------------------------------------------------------- #
    # Ending program
    # --------------------------------------------------------------------------- #
    logger.info('Ending program')
